//
//  CNContactStore+Updater.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-22.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation
import Contacts

extension CNContactStore {
    
    var keys : [CNKeyDescriptor] {
        get {
            return [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                    CNContactPhoneNumbersKey as CNKeyDescriptor]
        }
    }
    
    func requestAuthorization(success successBlock : @escaping () -> Void,
                                     failure failureBlock : @escaping () -> Void) {
        if self.isAuthorized(){
            successBlock()
        }else{
            self.requestAccess(for: .contacts, completionHandler: { (access, accessError) in
                if access{
                    successBlock()
                }else{
                    failureBlock()
                }
            })
        }
    }
    
    func isAuthorized() -> Bool {
        let authorizationStatus = CNContactStore.authorizationStatus(for: .contacts)
        return authorizationStatus == .authorized
    }
    
    func fetchContacts() -> [CNContact] {
        let fetchRequest = CNContactFetchRequest(keysToFetch: keys)
        fetchRequest.unifyResults = true

        var contacts = [CNContact]()
        do {
            try self.enumerateContacts(with: fetchRequest, usingBlock: { (contact, cursor) in
                contacts.append(contact)
            })
            return contacts
        }catch{
            return []
        }
    }
    
    func updateContact(contactIndentifier : String,
                              oldNumber : CNLabeledValue<CNPhoneNumber>,
                              newNumber : String) -> Bool {
        
        do {
            let contact = try self.unifiedContact(withIdentifier: contactIndentifier, keysToFetch: keys)
            guard let mutableContact = contact.mutableCopy() as? CNMutableContact else {
                return false
            }
            
            var newPhoneNumbers : [CNLabeledValue<CNPhoneNumber>] = []
            mutableContact.phoneNumbers.forEach({ (phoneNumber) in
                if phoneNumber.value.stringValue == oldNumber.value.stringValue {
                    newPhoneNumbers.append(CNLabeledValue(label: phoneNumber.label, value: CNPhoneNumber(stringValue: newNumber)))
                    return
                }
                newPhoneNumbers.append(phoneNumber)
            })
            
            mutableContact.phoneNumbers = newPhoneNumbers
            
            let saveRequest = CNSaveRequest()
            saveRequest.update(mutableContact)
            
            do {
                try self.execute(saveRequest)
                return true
            }catch{
                return false
            }
        } catch {
            return false
        }
    }
}
