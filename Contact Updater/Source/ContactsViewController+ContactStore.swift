//
//  ContactsViewController+ContactStore.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-23.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation
import Contacts
import UIKit

//MARK: Contacts management
extension ContactsViewController {
    
    func setupContacts(contactStore: CNContactStore) {
        if !contactStore.isAuthorized() {
            contactStore.requestAuthorization(success: {
                self.loadContacts(contactStore: contactStore)
            }, failure: {
                self.showAlert(message: "Your authorization is needed to perform the update.")
            })
        }
        else
        {
            loadContacts(contactStore: contactStore)
        }
    }
    
    func loadContacts(contactStore: CNContactStore) {
        self.contacts = [Contact]()
        contactStore.fetchContacts().forEach({ (cnContact) in
            for phoneNumber : CNLabeledValue<CNPhoneNumber> in cnContact.phoneNumbers {
                if let _ = self.newPhoneNumber(phoneNumber: phoneNumber) {
                    let newContact = Contact(identifier: cnContact.identifier,
                                             fullName: CNContactFormatter.string(from: cnContact, style: .fullName)! as String,
                                             phoneNumber: phoneNumber)
                    self.contacts?.append(newContact)
                }
            }
        })
        
        updateTableView()
        
        if contacts?.isEmpty == true {
            showAlert(message: "No contacts found.")
        }
    }
    
    func updateSelectedContacts(contactStore: CNContactStore,
                                selectedContacts : [IndexPath]?) -> [IndexPath] {
        guard let selectedContacts = selectedContacts else {
            showAlert(message: "There are no selected items to be updated.")
            return []
        }

        var modifiedContacts = [IndexPath]()
        selectedContacts.forEach { (indexPath) in
            guard   let contact = contacts?[indexPath.row],
                    let newNumber = newPhoneNumber(phoneNumber: contact.phoneNumber) else {
                return
            }
            
            if contactStore.updateContact(contactIndentifier: contact.identifier,
                                       oldNumber: contact.phoneNumber,
                                       newNumber: newNumber) {
                modifiedContacts.append(indexPath)
            }
        }
        return modifiedContacts
    }
    
    // MARK: Helper
    func newPhoneNumber(phoneNumber : CNLabeledValue<CNPhoneNumber>)->String? {
        guard   var number = stringNumber(phoneNumber: phoneNumber),
                let origin = self.origin,
                let destination = self.destination else {
            return nil
        }
        
        guard   number.count == cellNumberLength(country: origin)
            ||  number.count == homeNumberLength(country: origin) else {
                return nil
        }
        
        if let range = number.range(of: preNumber(country: origin)) {
            number.removeSubrange(range)
        }

        var newNumber = destination.internationalAccessCode
        if destination.hasCarrierCode, let carrierCode = destination.carrierCode {
            newNumber += carrierCode
        }
        newNumber += origin.countryCode
        newNumber += number
        
        return newNumber
    }
    
    func preNumberLength(country : Country) -> Int {
        return preNumber(country: country).count
    }
    
    func preNumber(country : Country) -> String {
        var preNumber = country.longDistanceCallCode ?? ""
        if let carrierCode = country.carrierCode, country.hasCarrierCode {
            preNumber += carrierCode
        }
        return preNumber
    }
    
    func cellNumberLength(country : Country) -> Int {
        return preNumberLength(country: country) + country.regionalCodeLength + country.cellPhoneLength
    }
    
    func homeNumberLength(country : Country) -> Int {
        return preNumberLength(country: country) + country.regionalCodeLength + country.homePhoneLength
    }

    func stringNumber(phoneNumber : CNLabeledValue<CNPhoneNumber>) -> String? {
        guard phoneNumber.value.stringValue.count > 0 else {
            return nil
        }
        return phoneNumber.value.stringValue.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    // MARK: Alert
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Error",
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK",
                                     style: .default,
                                     handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
