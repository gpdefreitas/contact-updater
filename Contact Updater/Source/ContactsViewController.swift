//
//  ContactsViewController.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-22.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import UIKit
import Contacts

class ContactsViewController: UIViewController {

    var origin: Country?
    var destination: Country?
    
    var contacts: [Contact]?
    private var contactStore = CNContactStore()
    
    private var updatedContacts = [IndexPath]()
    
    @IBOutlet private weak var contactsTableView: UITableView!
    @IBOutlet private weak var updateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateButton.isEnabled = false
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupContacts(contactStore: contactStore)
    }

    func updateTableView() {
        updatedContacts.removeAll()
        contactsTableView.reloadData()
        if let contacts = self.contacts {
            updateButton.isEnabled = contacts.count > 0
            selectAllContacts()
        }
    }
    
    //MARK: Private methods
    private func setupTableView()
    {
        contactsTableView.allowsMultipleSelection = true
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
    }
    
    private func selectAllContacts()
    {
        guard let contacts = self.contacts else {
            return
        }
        for (index, _) in contacts.enumerated() {
            contactsTableView.selectRow(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .bottom)
        }
    }
    
    @IBAction func updateContacts(_ sender: UIButton) {
        updatedContacts = updateSelectedContacts(contactStore: contactStore,
                                                 selectedContacts : contactsTableView.indexPathsForSelectedRows)
        updateButton.isEnabled = false
        contactsTableView.allowsSelection = false
        guard let indexPaths = contactsTableView.indexPathsForVisibleRows else {
            return
        }
        contactsTableView.reloadRows(at: indexPaths, with: .none)
    }
}

extension ContactsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectionChanged(tableView)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectionChanged(tableView)
    }
    
    func selectionChanged(_ tableView: UITableView) {
        guard let indexPaths = tableView.indexPathsForSelectedRows else {
            updateButton.isEnabled = false
            return
        }
        updateButton.isEnabled = indexPaths.count > 0
    }
}

extension ContactsViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = contacts?[indexPath.row].fullName ?? ""
        cell.accessoryType = updatedContacts.contains(indexPath) ? .checkmark : .none
        if let phoneNumber = contacts?[indexPath.row].phoneNumber, updatedContacts.contains(indexPath) {
            cell.detailTextLabel?.text = newPhoneNumber(phoneNumber: phoneNumber)
        }else{
            cell.detailTextLabel?.text = contacts?[indexPath.row].phoneNumber.value.stringValue ?? ""
        }
        return cell
    }
}
