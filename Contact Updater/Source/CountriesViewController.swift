//
//  CountriesViewController.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-22.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import UIKit

class CountriesViewController: UIViewController {

    //MARK: Properties
    private var countries : [Country]?
    
    //MARK: Computed properties
    private var originCountry : Country? {
        get {
            guard let selectedOriginCountries = selectedRowsInSection(countriesTableView, section: 0) else {
                return nil
            }
            guard let origin = selectedOriginCountries.first else {
                return nil
            }
            return countries?[origin.row]
        }
    }
    
    private var destinationCountry : Country? {
        get {
            guard let selectedDestinationCountries = selectedRowsInSection(countriesTableView, section: 1) else {
                return nil
            }
            guard let destination = selectedDestinationCountries.first else {
                return nil
            }
            return countries?[destination.row]
        }
    }
    
    @IBOutlet weak private var countriesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCountries()
        setupTableView()
    }

    // MARK: Private metods
    private func loadCountries()
    {
        if let jsonFile = Bundle.main.path(forResource: "countries", ofType: "json") {
            countries = DataService.retrieveData(from: jsonFile, using: [Country].self)
        }
    }
    
    private func setupTableView() {
        countriesTableView.allowsMultipleSelection = true
        countriesTableView.delegate = self
        countriesTableView.dataSource = self
    }
    
    // MARK: Helper
    private func selectedRowsInSection(_ tableView : UITableView, section: Int) -> [IndexPath]? {
        return tableView.indexPathsForSelectedRows?.filter{$0.section == section}
    }

    // MARK: Segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        guard let origin = originCountry, let destination = destinationCountry else {
            return false
        }

        return origin.countryCode == "55" && destination.countryCode == "1"

        // TODO: Restore when updated for other countries
        // return origin != destination
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let contatcsViewController = segue.destination as? ContactsViewController
        contatcsViewController?.origin = originCountry
        contatcsViewController?.destination = destinationCountry
    }
    
}

extension CountriesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedRows = selectedRowsInSection(tableView, section: indexPath.section) {
            selectedRows.forEach{
                if indexPath != $0 {
                    tableView.deselectRow(at: $0, animated: true)
                }
            }
        }
    }
}

extension CountriesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let country = countries?[indexPath.row] {
            cell.textLabel?.text = country.name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Set phone numbers from:" : "To call when you are in:"
    }
}
