//
//  Contact.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-23.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation
import Contacts

struct Contact {
    var identifier: String
    var fullName: String
    var phoneNumber: CNLabeledValue<CNPhoneNumber>
}
