//
//  Country.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-22.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation

struct Country : Codable {
    var name: String
    var countryCode: String
    var internationalAccessCode: String
    var longDistanceCallCode: String?
    var hasCarrierCode: Bool
    var carrierCode: String?
    var regionalCodeLength: Int
    var cellPhoneLength: Int
    var homePhoneLength: Int
}

extension Country : Equatable {
    static func ==(lhs: Country, rhs: Country) -> Bool {
        if      (lhs.carrierCode == nil && rhs.carrierCode != nil)
            ||  (lhs.carrierCode != nil && rhs.carrierCode == nil) {
            return false
        }
        
        if let lhsCarrierCodes = lhs.carrierCode, let rhsCarrierCodes = rhs.carrierCode {
            if lhsCarrierCodes != rhsCarrierCodes {
                return false
            }
        }
        
        return  lhs.name == rhs.name
            &&  lhs.countryCode == rhs.countryCode
            &&  lhs.internationalAccessCode == rhs.internationalAccessCode
            &&  lhs.longDistanceCallCode == rhs.longDistanceCallCode
            &&  lhs.hasCarrierCode == rhs.hasCarrierCode
            &&  lhs.regionalCodeLength == rhs.regionalCodeLength
            &&  lhs.cellPhoneLength == rhs.cellPhoneLength
            &&  lhs.homePhoneLength == rhs.homePhoneLength
    }

    static func != (lhs: Country, rhs: Country) -> Bool {
        return !(lhs==rhs)
    }
}
