//
//  DataService.swift
//  Contact Updater
//
//  Created by Guilherme de Freitas on 2018-03-22.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation

public class DataService {
    class func retrieveData<T : Decodable>(from jsonFile: String,
                                           using T : T.Type) -> T? {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: jsonFile),
                                options: .mappedIfSafe)
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            return nil
        }
    }
}
