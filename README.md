# Contact Updater


This app updates the phone numbers of your contacts using the pattern of the country you are moving to. 

Limitations:

1. This version only applies the Canadian/American pattern to Brazilian numbers; it doesn't do the opposite;
2. There are no other coutries included;
3. The Brazilian phone numbers follow this pattern:


- 1 digit (always 0) for long distance calls
- 2 digits for the carrier company to be used in long distance calls - the app considers only "15"
- 2 digits for region
- 8 digits for landlines or 9 for cellphones

------

Examples of Brazilian phone numbers:

* 0 15 55 30268669 (landline from the State of Rio Grande do Sul)
* 0 15 55 991078669 (cellphone from the State of Rio Grande do Sul)
* 0 15 11 898841122 (cellphone from the State of São Paulo)
* 0 15 21 21551135 (landline from the State of Rio de Janeiro)
* 0 15 61 884555321 (cellphone from Brasilia)

